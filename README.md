# Custom ESD foam for Licefa ESD box Koffer 2000-ESD

## Requirements
Designed using FreeCad 0.19.

## Manufacture
Manufactured using [de-pack.de](https://de-pack.de)

Price for 20: ca. 36 Euro/piece

Price for 200: ca. 16 Euro/piece

## Version log

v1.0: manufactured for CERN

v1.1: changed length and width from 
